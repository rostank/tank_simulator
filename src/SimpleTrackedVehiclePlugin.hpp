/*
 * Copyright (C) 2017 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef SIMPLETRACKEDVEHICLEPLUGIN_HPP
#define SIMPLETRACKEDVEHICLEPLUGIN_HPP

#include <string>
#include <unordered_map>

#include <boost/algorithm/string.hpp>

#include <ignition/gazebo/System.hh>


#include "plugins/TrackedVehiclePlugin.hh"


namespace tracks {

/// \class SimpleTrackedVehiclePlugin SimpleTrackedVehiclePlugin.hh
/// \brief A very fast, but also very accurate model of non-deformable tracks
///        without grousers.
/// \since 8.1
///
/// The motion model is based on adjusting motion1 in ODE contact properties
/// and on computing Instantaneous Center of Rotation for a tracked vehicle.
/// A detailed description of the model is given in
///  .
///
/// The plugin processes the following parameters, plus the common parameters
/// defined in TrackedVehiclePlugin.
///
/// <body>  Body of the vehicle to which the two tracks are connected.
/// <left_track>  The left track link's name.
/// <right_track>  The right track link's name.
/// <left_flipper>  The name of a left flipper link.
///     Can appear multiple times.
/// <right_flipper>  The name of a right flipper link.
///     Can appear multiple times.
/// <collide_without_contact_bitmask> Collision bitmask that will be set to
///     the whole vehicle (default is 1u).

class GZ_PLUGIN_VISIBLE SimpleTrackedVehiclePlugin:
      public ignition::gazebo::ISystemPreUpdate,
      public ignition::gazebo::ISystemUpdate {  
public:
  SimpleTrackedVehiclePlugin() = default;
  ~SimpleTrackedVehiclePlugin() override ;
  void PreUpdate(const ignition::gazebo::UpdateInfo &_info,
                 ignition::gazebo::EntityComponentManager &_ecm) override;
  void Update(const ignition::gazebo::UpdateInfo &_info,
              ignition::gazebo::EntityComponentManager &_ecm) override;
  
  /// \brief Return the number of tracks on the given side. Should always be
  /// at least 1 for the main track. If flippers are present, the number is
  /// higher.
  size_t GetNumTracks(Tracks side) const;

protected:

  
  /// \brief Set new target velocity for the tracks.
  ///
  /// \param[in] _left Velocity of left track.
  /// \param[in] _right Velocity of right track.
  void SetTrackVelocityImpl(double _left, double _right) override;

  /// \brief Update surface parameters of the tracks to correspond to the
  ///        values set in this plugin.
  void UpdateTrackSurface() override;

  /// \brief Compute and apply the forces that make the tracks move.
  void DriveTracks(const common::UpdateInfo &/*_unused*/);

  /// \brief Set collide categories and bits of all geometries to the
  ///        required values.
  ///
  /// This is a workaround for https://github.com/osrf/gazebo/issues/1855 .
  void SetGeomCategories();

  

  /// \brief Compute the velocity of the surface motion in all contact points.
  /// \param[in] _beltSpeed The desired belt speed.
  /// \param[in] _beltDirection Forward direction of the belt.
  /// \param[in] _frictionDirection First friction direction.
  double ComputeSurfaceMotion(double _beltSpeed,
                                       const ignition::math::Vector3d &_beltDirection,
                                       const ignition::math::Vector3d &_frictionDirection) const;

  
  /// \brief Compute the direction of friction force in given contact point.
  /// \param[in] _linearSpeed Linear speed of the vehicle.
  /// \param[in] _angularSpeed Angular speed of the vehicle.
  /// \param[in] _drivingStraight Whether the vehicle should steer.
  /// \param[in] _bodyPose Pose of the vehicle body.
  /// \param[in] _bodyYAxisGlobal Direction of the y-axis of the body in
  ///            world frame.
  /// \param[in] _centerOfRotation Center of the circle the vehicle
  ///            follows (Inf/-Inf if driving straight).
  /// \param[in] _contactWorldPosition World position of the contact point.
  /// \param[in] _contactNormal Corrected contact normal (pointing inside
  ///            the track).
  /// \param[in] _beltDirection World-frame forward direction of the belt.
  /// \return Direction of the friction force in world frame.
  ignition::math::Vector3d ComputeFrictionDirection(
    double _linearSpeed, double _angularSpeed,
    bool _drivingStraight, const ignition::math::Pose3d &_bodyPose,
    const ignition::math::Vector3d &_bodyYAxisGlobal,
    const ignition::math::Vector3d &_centerOfRotation,
    const ignition::math::Vector3d &_contactWorldPosition,
    const ignition::math::Vector3d &_contactNormal,
    const ignition::math::Vector3d &_beltDirection) const;

  /// \brief Body of the robot.
  physics::LinkPtr body;

  /// \brief The tracks controlled by this plugin.
  std::unordered_map<Tracks, physics::LinkPtr> tracks;

  /// \brief Desired velocities of the tracks.
  std::unordered_map<Tracks, double> trackVelocity;

  
  /// \brief This bitmask will be set to the whole vehicle body.
  unsigned int collideWithoutContactBitmask;
  /// \brief Category for the non-track parts of the robot.
  static const unsigned int ROBOT_CATEGORY = 0x10000000;
  /// \brief Category for tracks.
  static const unsigned int BELT_CATEGORY = 0x20000000;
  /// \brief Category for all items on the left side.
  static const unsigned int LEFT_CATEGORY = 0x40000000;

private:
  transport::NodePtr node;
  event::ConnectionPtr beforePhysicsUpdateConnection;
  physics::ContactManager *contactManager;
};
}

#endif
