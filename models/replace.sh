FILE=$1;   
echo "swapping $FILE";   
if [ -z $FILE ]; then
  echo "no file"
  return
fi

MOD=$(echo ${FILE} | cut -d "/" -f 2)
echo ${MOD}
sed  -i "s/<link name=\"link\">/<link name=\"${MOD}\">/g" ${FILE}
